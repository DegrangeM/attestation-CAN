# Attestation CAN

Attestation CAN est un outil pour générer des attestations de participation à la course au nombre au format numérique. Cette attestation est non officielle et est différente du diplome officiel délivré par les organisateurs de la CAN.

Cette attestation offre l'avantage d'être plus ludique et plus accessible grâce à un système de "badge/médailles" récompossants plusieurs types de réussites.

## Les différents badges / médailles

- 1er prix / 2ème prix / 3ème prix : comme pour le diplome officiel
- Coupe Or / Argent / Bronze : faire parti des 10% / 20% / 40% meilleurs score.
- Médaille Or / Argent / Bronze : avoir réussi une question qui n'a été réussie que par 10% / 20% / 40% des élèves
- Corde Or / Argent / Bronze : avoir réussi  8 / 6 / 4 questions d'affilés.
- Nombre premier d'or : avoir réussi une question dont le numéro est un nombre premier ?